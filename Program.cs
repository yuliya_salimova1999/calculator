﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Input();
            Console.ReadKey();
        }
        //Sum count function
        static double Sum(double[] input)
        {
            return input[0] + input[1];
        }
        //Substraction count function
        static double Substract(double[] input)
        {
            return input[0] - input[1];

        }
        // Multiplication count function
        static double Multiply(double[] input)
        {
            return input[0] * input[1];
        }

        // Devide count finction
        static double Devide(double[] input)
        {
            if (input[1] == 0)
            {
                Console.WriteLine("The second argument can't be 0");
                return 0;
            }
            else
            {
                return input[0] / input[1];
            }
        }

        // matrixes multiplication function
        static double[,] MultiplyMatrixes(double[,] a, double[,] b, int rowsA, int columnsA, int columnsB)
        {
            double[,] resultMatrix = new double[rowsA, columnsB];
            for (int rowA = 0; rowA < rowsA; rowA++)
            {
                for (int colB = 0; colB < columnsB; colB++)
                {
                    for (int colA = 0; colA < columnsA; colA++)
                    {
                        resultMatrix[rowA, colB] += a[rowA, colA] * b[colA, colB];
                    }
                }
            }
            return resultMatrix;
        }


        // Menu print function
        static void ShowMenu()
        {
            Console.WriteLine("1. Sum '+'\n2. Substract '-'\n3. Multiply '*'\n4. Devide '/'\n5. Multiply matrixes '[]'\n"+
                                "6. Use/Not use previosly saved value\n7. Show operations log\n8. Clear\n9. Exit '->'\n");
        }
        static void Input()
        {
            double previousValue = -10000;
            double[,] previousMatrixResult= null;
            bool shellState = true;
            bool toUsePreviousValue = false;
            List<string> operationsLog = new List<string>();
            while (shellState)
            {
                ShowMenu();
                Console.WriteLine("Choose an operation: ");
                string symbol = Console.ReadLine();
                switch (symbol)
                {
                    case "1":
                        previousValue=BinaryOperationInputOutput("+", toUsePreviousValue, previousValue);
                        operationsLog.Add("Sum '+'");
                        break;
                    case "2":
                        previousValue = BinaryOperationInputOutput("-", toUsePreviousValue, previousValue);
                        operationsLog.Add("Substract '-'");
                        break;
                    case "3":
                        previousValue = BinaryOperationInputOutput("*", toUsePreviousValue, previousValue);
                        operationsLog.Add("Multiply '*'");
                        break;
                    case "4":
                        previousValue = BinaryOperationInputOutput("/", toUsePreviousValue, previousValue);
                        operationsLog.Add("Devide '/'");
                        break;
                    case "5":
                        InputTypeValidation validation = null;
                        validation = IntInputTypeValidaton;
                        int rowsA = 0;
                        int colA = 0;
                        double[,] matrixA = null;
                        if (toUsePreviousValue && previousMatrixResult != null)
                        {
                            PrintMatrix(previousMatrixResult, "A");
                            rowsA = previousMatrixResult.GetUpperBound(0) + 1;
                            colA = previousMatrixResult.Length / rowsA;
                            matrixA = previousMatrixResult;
                        }
                        else 
                        {
                            Console.WriteLine("Enter number of rows(r) and columns(col) of the first matrix:");
                            Console.WriteLine("(r)");
                            rowsA = (int)MatrixRowsAndColumnsValidation(validation);
                            Console.WriteLine("(col)");
                            colA = (int)MatrixRowsAndColumnsValidation(validation);
                            Console.WriteLine("Fill the first matrix r = " + rowsA.ToString() + ", col = " + colA.ToString());
                            matrixA = MatrixInput(rowsA, colA, "a");
                        }
                        Console.WriteLine("Enter number of rows(r) and columns(col) of the second matrix:");
                        Console.WriteLine("(r)");
                        int rowsB = (int)MatrixRowsAndColumnsValidation(validation);
                        Console.WriteLine("(col)");
                        int colB = (int)MatrixRowsAndColumnsValidation(validation);
                        if (colA == rowsB)
                        {
                            Console.WriteLine("Fill the second matrix r = " + rowsB.ToString() + ", col = " + colB.ToString());
                            double[,] matrixB = MatrixInput(rowsB, colB, "b");
                            double[,] resultMatrix = MultiplyMatrixes(matrixA, matrixB, rowsA, colA, colB);
                            previousMatrixResult = resultMatrix;
                            operationsLog.Add("Multiply matrixes '[]'");
                            PrintMatrix(matrixA, " A");
                            PrintMatrix(matrixB, " B");
                            PrintMatrix(resultMatrix, " A * B = C");
                        }
                        else 
                        {
                            Console.WriteLine("The number of columns of matrix A doesn't match the number of rows of matrix B!\n" +
                                                "Multiplication of matrixes can't be executed!");
                        }
                        break;
                    case "6":
                        if (toUsePreviousValue)
                        {
                            toUsePreviousValue = false;
                            Console.WriteLine("PREVIOUSLY SAVED VALUE USE MODE IS DIACTIVATED!\n");
                        }
                        else 
                        {
                            toUsePreviousValue = true;
                            Console.WriteLine("PREVIOUSLY SAVED VALUE USE MODE IS ACTIVATED!\n");
                        }
                        break;
                    case "7":
                        if (operationsLog != null)
                        {
                            foreach (string oper in operationsLog)
                            {
                                Console.WriteLine(oper);
                            }
                        }
                        else 
                        {
                            Console.WriteLine("No operations have been executed yet!");
                        }
                        break;
                    case "8":
                        Console.Clear();
                        break;
                    case "9":
                        Environment.Exit(0);
                        break;
                }
            }
        }

        static double[] BinaryOperationsInput(bool toUsePrevVal, double prevVal)
        {
            InputTypeValidation validation = DoubleInputTypeValidaton;
            double[] arguments = new double[2];
            if (toUsePrevVal && prevVal!=-10000)
            {
                Console.WriteLine("The first argument = " + prevVal.ToString());
                arguments[0] = prevVal;
            }
            else 
            {
                Console.WriteLine("Enter the first argument: ");
                arguments[0] = (double)ReturnValidatedValue(validation);
            }
            Console.WriteLine("Enter the second argument: ");
            arguments[1] = (double)ReturnValidatedValue(validation);
            return arguments;
        }

        delegate object InputTypeValidation(string input);

        static object ReturnValidatedValue(InputTypeValidation validation) 
        {
            string input = Console.ReadLine();
            object result = validation(input);
            if (result == null)
            {
                Console.WriteLine("Wrong input! Try again: ");
                return ReturnValidatedValue(validation);
            }
            else return result;
        }

        static object DoubleInputTypeValidaton(string input) 
        {
            double argument;
            char[] separators = { ' ', '-' };
            if (input.Split(separators, 10).Length > 1) 
            {
                return null;
            }
            bool parseSuccess = double.TryParse(input,out argument);
            if (parseSuccess)
            {
                return argument;
            }
            else 
            {
                return null;
            }
        }
        static object IntInputTypeValidaton(string input)
        {
            int argument;
            char[] separators = { ' ', '-' };
            if (input.Split(separators, 10).Length > 1)
            {
                return null;
            }
            bool parseSuccess = int.TryParse(input.Split(separators, 10)[0], out argument);
            if (parseSuccess)
            {
                return argument;
            }
            else
            {
                return null;
            }
        }
        static double BinaryOperationInputOutput(string ch, bool toUsePrevVal, double prevVal)
        {
            double[] arguments = BinaryOperationsInput(toUsePrevVal, prevVal);
            double result = Sum(arguments);
            switch (ch)
            {
                case "+":
                    result = Sum(arguments);
                    break;
                case "-":
                    result = Substract(arguments);
                    break;
                case "*":
                    result = Multiply(arguments);
                    break;
                case "/":
                    result = Devide(arguments);
                    break;

            }
            Console.WriteLine("\n---------------------------");
            Console.WriteLine(ReturnStringArguments(arguments)[0] +" "+ ch + " " + ReturnStringArguments(arguments)[1] + " = " + result.ToString());
            Console.WriteLine("---------------------------\n");
            return result;
        }
        static string[] ReturnStringArguments(double[] inputArguments)
        {
            string[] stringArguments = new string[2];
            stringArguments[0] = inputArguments[0].ToString();
            stringArguments[1] = inputArguments[1].ToString();
            return stringArguments;
        }
        static double MatrixRowsAndColumnsValidation(InputTypeValidation validation) 
        {
            int input = (int)ReturnValidatedValue(validation);
            if (input < 1)
            {
                Console.WriteLine("Enter the number >= 1");
                return MatrixRowsAndColumnsValidation(validation);
            }
            else return input;
        }
        static double[,] MatrixInput(int rowsA, int colA, string elementName)
        {
            InputTypeValidation validation = DoubleInputTypeValidaton;
            double[,] matrix = new double[rowsA,colA];
            for (int row = 0; row < rowsA; row++)
            {
                for (int col = 0; col < colA; col++)
                {
                    Console.WriteLine("Enter " + elementName + (row + 1).ToString() + (col + 1).ToString() + ":");
                    matrix[row, col] = (double)ReturnValidatedValue(validation);
                }

            }
            return matrix;
        }
        static void PrintMatrix(double[,] a, string ch) 
        {
            int rows = a.GetUpperBound(0) + 1;
            int columns = a.Length / rows;
            Console.WriteLine("Matrix"+ch);
            Console.WriteLine("\n---------------------------");
            for (int row = 0; row < rows; row++) 
            {
                for (int col = 0; col < columns; col++) 
                {
                    if (col == columns - 1)
                    {
                        Console.Write(a[row, col].ToString() + "\n");
                    }
                    else 
                    {
                        Console.Write(a[row, col].ToString() + " ");
                    }
                }
            }
            Console.WriteLine("---------------------------\n");
        }
    }
}
